package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.adapter.PlacesListAdapter;
import za.ac.myuct.klmedu001.pulsecheck.constant.Constants;
import za.ac.myuct.klmedu001.pulsecheck.fragment.ExploreFragment;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerFragment;
import za.ac.myuct.klmedu001.pulsecheck.web.ExplorePulsesLoader;

/**
 * Created by eduardokolomajr on 2015/04/16.
 * TODO add comment here
 */
@Module
public class ExploreFragmentModule{
    private final String TAG = getClass().getSimpleName();
    private final ExploreFragment fragment;

    public ExploreFragmentModule(ExploreFragment fragment) {
        this.fragment = fragment;
    }

    @Provides @PerFragment
    public ExploreFragment providesExploreFragment(){
        return fragment;
    }

    @Provides @PerFragment
    public PlacesListAdapter providesPlacesListAdapter(){
        return new PlacesListAdapter(fragment.getActivity());
    }

    @Provides @PerFragment
    public RecyclerView.LayoutManager providesLayoutManager(ExploreFragment fragment){
        if(fragment.getResources().getBoolean(R.bool.isTablet)) {
            Log.d(TAG, "####returning staggered layout");
            StaggeredGridLayoutManager lm = new StaggeredGridLayoutManager(Constants.STAGGERED_GRID_SPAN, StaggeredGridLayoutManager.VERTICAL);
            lm.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
            return lm;
        }else {
            Log.d(TAG, "#####returning linear layout");
            return new LinearLayoutManager(fragment.getActivity());
        }
    }

    @Provides @PerFragment
    public ExplorePulsesLoader providesExplorePulsesLoader(){
        return new ExplorePulsesLoader(Constants.EXPLORE_URL);
    }


}
