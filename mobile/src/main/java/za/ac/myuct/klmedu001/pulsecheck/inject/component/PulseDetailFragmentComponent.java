package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.LocationOverviewModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.PulseDetailFragmentModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerFragment;
import za.ac.myuct.klmedu001.pulsecheck.web.PulseDetailsLoader;

/**
 * Dagger component for {@link za.ac.myuct.klmedu001.pulsecheck.fragment.PulseDetailFragment}
 * Created by eduardokolomajr on 2015/05/03.
 */
@PerFragment
@Component(dependencies = LocationOverviewModule.class, modules = PulseDetailFragmentModule.class)
public interface PulseDetailFragmentComponent {
    PulseDetailsLoader pulseDetailsLoader();
}
