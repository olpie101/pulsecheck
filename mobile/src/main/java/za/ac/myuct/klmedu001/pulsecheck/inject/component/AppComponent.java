package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import android.content.Context;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.AppModule;

/**
 * Links {@link za.ac.myuct.klmedu001.pulsecheck.constant.App} and {@link AppModule} for Dagger dependency injection.<br/>
 * Created by eduardokolomajr on 2015/04/14.
 */
@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    Context context();

    Bus bus();
}
