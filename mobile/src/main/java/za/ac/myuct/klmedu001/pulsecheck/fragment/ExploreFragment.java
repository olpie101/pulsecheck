package za.ac.myuct.klmedu001.pulsecheck.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import rx.Observable;
import rx.android.app.AppObservable;
import rx.android.app.support.RxFragment;
import rx.android.lifecycle.LifecycleEvent;
import rx.android.lifecycle.LifecycleObservable;
import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity;
import za.ac.myuct.klmedu001.pulsecheck.activity.PulseDetailActivity;
import za.ac.myuct.klmedu001.pulsecheck.adapter.PlacesListAdapter;
import za.ac.myuct.klmedu001.pulsecheck.constant.App;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.MainActivityComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.DaggerExploreComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.ExploreComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.ExploreFragmentModule;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;
import za.ac.myuct.klmedu001.pulsecheck.otto.PulseClickedEvent;
import za.ac.myuct.klmedu001.pulsecheck.web.ExplorePulsesLoader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExploreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ExploreFragment extends RxFragment {
    private final String TAG = getClass().getSimpleName();

    @InjectView(R.id.rv_explore)
    RecyclerView recyclerView;

    @Inject PlacesListAdapter pla;
    @Inject ExplorePulsesLoader explorePulsesLoader;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ExploreFragment.
     */
    public static ExploreFragment newInstance() {
        return new ExploreFragment();
    }

    public ExploreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.inject(this, v);
        getFragmentComponent().inject(this);
        recyclerView.setLayoutManager(getFragmentComponent().layoutManager());
        recyclerView.setAdapter(pla);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadPulses();
    }

    private void loadPulses() {
        bindLifecycle(explorePulsesLoader.getLocations(), LifecycleEvent.DESTROY_VIEW)
                .subscribe(
                        pla::addItem,
                        error -> {
                            pla.notifyDataSetChanged();
                            toast("Error loading pulses");
                        },
                        pla::notifyDataSetChanged);
    }

    private void toast(String message) {
        Toast.makeText(this.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    private MainActivityComponent getActivityComponent(){
        return ((MainActivity)getActivity()).getActivityComponent();
    }

    private ExploreComponent getFragmentComponent(){
        return DaggerExploreComponent.builder()
                .mainActivityComponent(getActivityComponent())
                .exploreFragmentModule(new ExploreFragmentModule(this))
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((App)getActivityComponent().activity().getApplicationContext()).getAppComponent().bus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ((App)getActivityComponent().activity().getApplicationContext()).getAppComponent().bus().unregister(this);
    }

    /**
     * Bind observable to fragment lifecycle. Ensures that observable only emits as long a fragment
     * is in a valid state
     * @param observable Observable to bind
     * @param lifecycleEvent bind until given lifecyle event
     * @param <T> Type provided observable shall emit
     * @return Observable based on given observable bound until given lifecycle
     */
    private <T> Observable<T> bindLifecycle(Observable<T> observable, LifecycleEvent lifecycleEvent){
        Observable<T> obs = AppObservable.bindFragment(this, observable);
        return LifecycleObservable.bindUntilLifecycleEvent(lifecycle(), obs, lifecycleEvent);
    }

    @Subscribe
    public void onPulseClicked(PulseClickedEvent evt){
        LocationOverview pulse = pla.getItem(evt.position);
        Intent intent = new Intent(getActivity(), PulseDetailActivity.class);
        intent.putExtra("pulse", pulse);
        getActivity().startActivity(intent);
    }
}
