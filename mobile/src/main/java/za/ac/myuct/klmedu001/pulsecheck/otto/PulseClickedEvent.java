package za.ac.myuct.klmedu001.pulsecheck.otto;

/**
 * Otto event for when a pulse is clicked
 * Created by eduardokolomajr on 2015/04/27.
 */
public class PulseClickedEvent {
    public final int position;

    public PulseClickedEvent(int position) {
        this.position = position;
    }
}
