package za.ac.myuct.klmedu001.pulsecheck.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnPageChange;
import rx.Observable;
import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.adapter.MainActivitySectionsAdapter;
import za.ac.myuct.klmedu001.pulsecheck.constant.App;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.DaggerMainActivityComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.MainActivityComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.AppComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.MainActivityModule;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
    String TAG = "MainActivity";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    @Inject
    MainActivitySectionsAdapter mSectionsPagerAdapter;

    @InjectView(R.id.pager)
    ViewPager mViewPager;
    @Inject ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TODO Use Dagger to inject adapters, fragments and actionbar
        //TODO use RxJava and RxAndroid and RetroLambda for loops and async tasks
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        //Inject required variables
        //Log.d(TAG, "about to inject");
        getActivityComponent().inject(this);

//        MainActivitySectionsAdapter masa = new MainActivitySectionsAdapter(this.getSupportFragmentManager(), Constants.MAINACTIVITY_FRAGMENTS);

        // Set up the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // For each of the sections in the app, add a tab to the action bar.
        Observable.range(0, mSectionsPagerAdapter.getCount())
                .subscribe(this::addActionBarTab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAppComponent().bus().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getAppComponent().bus().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }


    @OnPageChange(R.id.pager)
    public void onPageSelected(int position) {
        Log.d(TAG, "Pager switched");
        actionBar.setSelectedNavigationItem(position);
    }

    private AppComponent getAppComponent(){
        return ((App)getApplication()).getAppComponent();
    }

    private Context getAppContext(){
        return getAppComponent().context();
    }

    public MainActivityComponent getActivityComponent() {
        return DaggerMainActivityComponent.builder().appComponent(getAppComponent())
                .mainActivityModule(new MainActivityModule(this)).build();
    }

    private void toast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void addActionBarTab(int i){
        //Log.d(TAG, "tab added with number "+i);
        actionBar.addTab(
                actionBar.newTab()
                        .setText(mSectionsPagerAdapter.getPageTitle(i))
                        .setTabListener(this));
    }
}
