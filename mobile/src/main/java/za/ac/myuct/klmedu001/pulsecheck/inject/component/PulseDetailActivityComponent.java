package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.activity.PulseDetailActivity;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.PulseActivityModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerActivity;

/**
 * Component for {@link PulseDetailActivity}
 * Created by eduardokolomajr on 2015/04/27.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = PulseActivityModule.class)
public interface PulseDetailActivityComponent {
    void inject(PulseDetailActivity pulseDetailActivity);

    //Exposed to sub-graphs.
    Activity activity();

    ActionBar actionBar();

    FragmentManager fragmentManager();
}
