package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;

/**
 * dagger module for {@link za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview}
 * Created by eduardokolomajr on 2015/05/03.
 */
@Module
public class LocationOverviewModule {
    private final LocationOverview pulse;

    public LocationOverviewModule(LocationOverview pulse) {
        this.pulse = pulse;
    }

    @Provides public LocationOverview providePulse(){ return pulse;}
}
