package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.constant.Constants;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerActivity;
import za.ac.myuct.klmedu001.pulsecheck.util.EntryPair;

/**
 * Module used for dependency injection for {@link za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity}
 * Created by eduardokolomajr on 2015/04/14.
 */
@Module
public class MainActivityModule {
    private final static String TAG = "ActivityModule";
    private final Activity activity;

    public MainActivityModule(Activity activity) {
        Log.d(TAG, "actvity module created");
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity() {
        Log.d(TAG, "actvity requested");
        return this.activity;
    }

    @Provides @PerActivity
    ActionBar getSupportActionBar(){
            Log.d(TAG, "AB requested");
            return ((AppCompatActivity)activity).getSupportActionBar();
    }

    @Provides @PerActivity
    FragmentManager getSupportFragmentManager(){
        Log.d(TAG, "FragMan requested");
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @Provides @PerActivity
    List<EntryPair<String, Class>> getSectionsList(){
        Log.d(TAG, "Sections list requested");
        return Constants.MAINACTIVITY_FRAGMENTS;
    }
}
