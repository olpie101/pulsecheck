package za.ac.myuct.klmedu001.pulsecheck.web;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;

/**
 * Loads the details for a load a specific pulse(location)
 * Created by eduardokolomajr on 2015/05/02.
 */
public class PulseDetailsLoader {
    private final String TAG = getClass().getSimpleName();

    private final LocationOverview pulse;

    private Document doc = new Document("");
    private DOMLoadedFuture DOMLoaded;
    private Context ctx;

    /**
     * Creates a pulse detail loader. Loads details DOM on BG thread
     * @param pulse to load details about
     */
    public PulseDetailsLoader(LocationOverview pulse) {
        this.pulse = pulse;

        Observable<String> docObservable = Observable.just(pulse.link)
                .subscribeOn(Schedulers.io());

        docObservable.subscribe(new UrlObserver());
        DOMLoaded = new DOMLoadedFuture();
    }

    /**
     * Provides an observable with the pulse's description
     * @return description
     */
    public Observable<String> getDescription(){
        Log.d(TAG, "Getting description for pulse");

        return Observable.just(DOMLoaded)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(getElement(".Detail_paragraph p", 0))
                .map(Element::ownText);
    }

    /**
     * Provides an observable with the pulse's number of followers
     * @return number of followers
     */
    public Observable<Integer> getFollowers(){
        return Observable.just(DOMLoaded)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(getElement(".followers h1", 0))
                .map(element -> Integer.parseInt(element.text()));
    }

    /**
     * provides and observable with pulse's google maps URL
     * @return googlr maps URL
     */
    public Observable<LatLng> getMapCoords(){
        return Observable.just(DOMLoaded)
                .subscribeOn(Schedulers.io())
                .map(getElement(".col-md-4 iframe", 0))
//                .map(element -> element.select(".google-maps-link").get(0))
//                .map(element -> {
//                    Log.d(TAG, "after getting link =>" + element.outerHtml());
//                    return element.attr("src");
//                })
//                .subscribeOn(Schedulers.io())
                .map(loadMapsHtml())
                .map(Jsoup::parse)
                .map(document -> document.select("script").get(2).html())
                .map(getCoordsFromScript())
                .map(stringCoords -> stringCoords.split(","))
                .map(splitCoords -> new LatLng(Double.parseDouble(splitCoords[0]), Double.parseDouble(splitCoords[1])))
                .observeOn(AndroidSchedulers.mainThread());
//        initEmbed([null,null,null,null,null,[[[2,"spotlight",null,null,null,null,null,[null,["0x1dcc677c90fa4f93:0xfd404146d30cb110","36 Buitenkant St, Cape Town City Centre, Cape Town, 8000, South Africa",null,[null,null,-33.92819,18.42256],0,1],null,null,null,null,null,null,null,"36 Buitenkant St",null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,1],1]]],[[52]]],null,["en_US"],[null,"/maps/api/js/ApplicationService.UpdateStarring","//accounts.google.com/ServiceLogin?continue=https://www.google.com/maps/api/js/ApplicationService.AuthSuccess","/maps/api/js/ApplicationService.GetEntityDetails","/maps/embed/upgrade204",null,"/maps/embed/record204","/maps/api/js/ApplicationService.Search"],null,null,null,null,null,null,null,null,"WO5HVZj4G8SBU8i8gbgM",null,null,null,[[[3310.502856238704,18.42256,-33.92819],null,null,13.10000038146973],null,0,[["0x1dcc677c90fa4f93:0xfd404146d30cb110","36 Buitenkant St, Cape Town City Centre, Cape Town, 8000, South Africa",[-33.92819,18.42256]],"36 Buitenkant St",["Cape Town City Centre, Cape Town","8000, South Africa"],null,null,null,null,null,null,null,null,null,null,"36 Buitenkant St, Cape Town City Centre, Cape Town, 8000, South Africa",null,null,[[[null,[2,"5ZLDsbMQ3hm_pnThkt4XDg"]],[[3,18.42256,-33.92819],[326,90,0],[1000,1000],90],["//geo0.ggpht.com/cbk?cb_client=unknown_client\u0026output=thumbnail\u0026thumb=2\u0026panoid=5ZLDsbMQ3hm_pnThkt4XDg\u0026w=374\u0026h=75\u0026yaw=326\u0026pitch=0\u0026thumbfov=120\u0026ll=-33.928190,18.422560","Street View",null,[374,75]],1]],null,null,null,null,null,null,1,1]],null,null,[null,null,[0],null,null,[0],null,[0],[0],[0]],0]);
    }

    private Func1<String, String> getCoordsFromScript() {
        return script -> {
            int start = script.indexOf("ll=")+3;
            int end = script.indexOf("\",", start);
            return script.substring(start, end);
        };
    }

    /**
     * Loads google maps embedded api and locates LatLng coordinates
     * @return string containing embedded maps iframe. This is later used to extract coordinates
     */
    private Func1<Element, String> loadMapsHtml(){
        return element -> {
            HttpClient httpclient = new DefaultHttpClient();

            // Prepare a request object
            HttpGet httpGet = new HttpGet(element.attr("src"));

            // Execute the request
            HttpResponse response;
            StringBuilder sb = new StringBuilder();
            try {
                response = httpclient.execute(httpGet);

                // Get hold of the response entity
                HttpEntity entity = response.getEntity();

                // If the response does not enclose an entity, there is no need
                // to worry about connection release
                if (entity != null) {
                    InputStream inStream = entity.getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

                    String line = null;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // now you have the string representation of the HTML request
                    inStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return sb.toString();
        };
    }
    /**
     * retrieves a a specific element from the doc
     * @param cssQuery query to search for
     * @param position position of particular element to return
     * @return {@link Element} requested from doc
     */
    private Func1<DOMLoadedFuture, Element> getElement(String cssQuery, int position) {
        return document ->{
            try {
                Element el = document.get().select(cssQuery).get(position);
                Log.d(TAG, "return for element =>"+cssQuery);
                return el;
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        };
    }

    /**
     * Observable for the pulse URL. Used to get the DOM on a separate thread off the ui.
     */
    private class UrlObserver implements Observer<String> {

        @Override
        public void onCompleted() {Log.d(TAG, "########DONE LOADING DOC!!");}

        @Override
        public void onError(Throwable e) {
            Log.d(TAG, "Error  getting url to load DOM");
            e.printStackTrace();
        }

        @Override
        public void onNext(String url) {
            try {
                doc = Jsoup.connect(url).get();
                DOMLoaded.done = true;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG, "Error loading DOM for pulse");
            }
        }
    }

    private class DOMLoadedFuture implements Future<Document>{
        boolean done = false;
        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return true;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return done;
        }

        @Override
        public Document get() throws InterruptedException, ExecutionException {
            Log.d(TAG, "Getting DOM");
            long startTime = System.currentTimeMillis();
            while (!done && System.currentTimeMillis() - startTime < 10000)
                continue;

            return doc;
        }

        @Override
        public Document get(long timeout, @NonNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            long startTime = System.currentTimeMillis();
            while(!done && System.currentTimeMillis() - startTime < unit.toMillis(timeout))
                continue;

            return doc;
        }
    }
}
