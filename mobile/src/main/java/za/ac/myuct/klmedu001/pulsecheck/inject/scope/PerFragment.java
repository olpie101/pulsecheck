package za.ac.myuct.klmedu001.pulsecheck.inject.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by eduardokolomajr on 2015/04/16.
 * Scope for per {@link android.support.v4.app.Fragment} basis
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerFragment {
}
