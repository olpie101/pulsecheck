package za.ac.myuct.klmedu001.pulsecheck.inject.scope;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by eduardokolomajr on 2015/04/14.
 * Scope for per {@link android.support.v7.app.ActionBarActivity} basis
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {}
