package za.ac.myuct.klmedu001.pulsecheck.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.constant.App;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;
import za.ac.myuct.klmedu001.pulsecheck.otto.PulseClickedEvent;

/**
 * Adapter for {@link android.support.v7.widget.RecyclerView} widgets in the
 * {@link za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity}
 * Created by eduardokolomajr on 2015/04/16.
 */
public class PlacesListAdapter extends RecyclerView.Adapter<PlacesListAdapter.ViewHolder>{
    private List<LocationOverview> locations;
    private static final String TAG = "PlacesListAdapter";
    private WeakReference<Context> context;

    @Inject
    public PlacesListAdapter(Context context) {
        this.context = new WeakReference<>(context);
        locations = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_place, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //check if a phone of tablet and respond accordingly
        holder.title.setText(locations.get(position).title);

        //get cardview height from resource and estimate width from that
        int imageHeight = context.get().getResources().getDimensionPixelSize(R.dimen.cardview_places_height);
        int imageWidth = (int)(imageHeight*1.5); //TODO figure out how to calculate final width of a particular object

        Picasso.with(context.get()).load(locations.get(position).imageLink)
                .resize(imageWidth, imageHeight).centerCrop().into(holder.placeImage);

        if(context.get().getResources().getBoolean(R.bool.isTablet)){
            //TODO check smallest width for tablets
            if(position == 0)
                ((StaggeredGridLayoutManager.LayoutParams)holder.itemView.getLayoutParams()).setFullSpan(true);
            else
                ((StaggeredGridLayoutManager.LayoutParams)holder.itemView.getLayoutParams()).setFullSpan(false);
        }

        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    /**
     * Adds given location to locations list. This method does not notify of a data set change
     * @param location location to add to list
     */
    public void addItem(LocationOverview location){
        locations.add(location);
    }

    public LocationOverview getItem(int position){
        if(position < 0 || position >= locations.size())
            return null;

        return locations.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        int position;
        @InjectView(R.id.iv_cardview_places)
        ImageView placeImage;
        @InjectView(R.id.tv_cardview_title)
        TextView title;
        public ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }

        @OnClick(R.id.cardview_places)
        public void cardClicked(){
            ((App)context.get().getApplicationContext()).getAppComponent().bus()
                    .post(new PulseClickedEvent(position));
        }
    }


}
