package za.ac.myuct.klmedu001.pulsecheck.constant;

import java.util.Arrays;
import java.util.List;

import za.ac.myuct.klmedu001.pulsecheck.fragment
        .ExploreFragment;
import za.ac.myuct.klmedu001.pulsecheck.util.EntryPair;

/**
 * Contains constants that can be used throughout the app
 * Created by eduardokolomajr on 2015/04/16.
 */
public class Constants {
    public static final String EXPLORE_FRAGMENT_TITLE = "Explore";
    public static final List<EntryPair<String, Class>> MAINACTIVITY_FRAGMENTS=
            Arrays.asList(new EntryPair<>(EXPLORE_FRAGMENT_TITLE, ExploreFragment.class));

    public static final int STAGGERED_GRID_SPAN = 2;

    public static final String BASE_URL = "http://pulsecheck.co.za";
    public static final String EXPLORE_URL = BASE_URL +"/CapeTown/Explore";
}
