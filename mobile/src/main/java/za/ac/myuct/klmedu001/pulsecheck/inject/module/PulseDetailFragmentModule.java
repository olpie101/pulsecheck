package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.fragment.PulseDetailFragment;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerFragment;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;
import za.ac.myuct.klmedu001.pulsecheck.web.PulseDetailsLoader;

/**
 * Module for {@link za.ac.myuct.klmedu001.pulsecheck.fragment.PulseDetailFragment}
 * Created by eduardokolomajr on 2015/05/02.
 */
@PerFragment
@Module
public class PulseDetailFragmentModule {
    private final String TAG = getClass().getSimpleName();
    private final PulseDetailFragment fragment;

    public PulseDetailFragmentModule(PulseDetailFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    public PulseDetailFragment provideFragment(){return fragment;}

    @Provides
    @PerFragment
    public PulseDetailsLoader providePulseDetailLoader(LocationOverview pulse){
        return new PulseDetailsLoader(pulse);
    }
}
