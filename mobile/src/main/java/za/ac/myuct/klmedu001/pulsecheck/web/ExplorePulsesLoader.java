package za.ac.myuct.klmedu001.pulsecheck.web;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;

/**
 * Rx class to load explore nodes
 * Created by eduardokolomajr on 2015/04/25.
 */
public class ExplorePulsesLoader {
    private final String TAG = this.getClass().getSimpleName();
    private final String baseurl;

    public ExplorePulsesLoader(String baseurl) {
        this.baseurl = baseurl;
    }

    public Observable<LocationOverview> getLocations(){
        Log.d(TAG, "locations requested");

        return Observable.just(baseurl)
                .subscribeOn(Schedulers.io())
                .map(getDoc())
                .flatMap(getElements())
                .map(createLocationOverview());
    }

    public Func1<Document, Observable<Element>> getElements(){
        return doc -> Observable.from(doc.select(".Pulse_List a"));
    }

    public Func1<String, Document> getDoc(){
        return url -> {
            try {
                return Jsoup.connect(url).get();
            } catch (IOException e) {
                Log.d(TAG, "Error getting DOM for explore pulses");
            }
            return null;
        };
    }

    public Func1<Element, LocationOverview> createLocationOverview(){
        return element -> {
            Element imgNode = element.select("img").first();
            Element titleNode = element.select(".pre-overlay").first();
            String title = titleNode.text();
            String link = "http://pulsecheck.co.za"+element.attr("href");
            String imgLink = "http://pulsecheck.co.za"+imgNode.attr("src");
            return new LocationOverview(title, link, imgLink);
        };
    }
}
