package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerActivity;

/**
 * Module for {@link za.ac.myuct.klmedu001.pulsecheck.activity.PulseDetailActivity}
 * Created by eduardokolomajr on 2015/04/27.
 */
@PerActivity
@Module
public class PulseActivityModule {
    public final Activity activity;

    public PulseActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }

    @Provides @PerActivity
    ActionBar getSupportActionBar(){
        return ((AppCompatActivity)activity).getSupportActionBar();
    }

    @Provides @PerActivity
    FragmentManager getSupportFragmentManager(){
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }
}
