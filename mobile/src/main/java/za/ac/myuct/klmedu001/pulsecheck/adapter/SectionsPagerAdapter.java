package za.ac.myuct.klmedu001.pulsecheck.adapter;

/**
 * Adapter for {@link android.support.v4.view.ViewPager} in {@link za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity}
 * Created by eduardokolomajr on 2015/04/16.
 */

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;
import java.util.Locale;

import za.ac.myuct.klmedu001.pulsecheck.util.EntryPair;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public abstract class SectionsPagerAdapter extends FragmentPagerAdapter {
    public static final String TAG = "SectionsPagerAdapter";
    protected final List<EntryPair<String, Class>> fragments;

    public SectionsPagerAdapter(FragmentManager fm, List<EntryPair<String, Class>> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        if(position > -1 && position < fragments.size())
            return fragments.get(position).first.toUpperCase(l);

        return null;
    }
}
