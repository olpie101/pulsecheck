package za.ac.myuct.klmedu001.pulsecheck.util;

/**
 * Entry Pair. Helps facilitate management with {@link android.support.v4.app.FragmentPagerAdapter}
 * Created by eduardokolomajr on 2015/04/16.
 */
public class EntryPair<F, S> {
    public final F first;
    public final S second;

    public EntryPair(F first, S second) {
        this.first = first;
        this.second = second;
    }
}
