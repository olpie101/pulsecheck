package za.ac.myuct.klmedu001.pulsecheck.constant;

import android.app.Application;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import za.ac.myuct.klmedu001.pulsecheck.inject.module.AppModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.AppComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.DaggerAppComponent;

/**
 * Created by eduardokolomajr on 2015/04/13.
 * App class used to create the start of the Dagger ObjectGraph
 */
@Singleton
public class App extends Application{
    private AppComponent appComponent;
    private Bus mainEventBus;

    @Override
    public void onCreate() {
        super.onCreate();
        initializeInjector();
        mainEventBus = appComponent.bus();
    }

    private void initializeInjector() {
        this.appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return this.appComponent;
    }
}
