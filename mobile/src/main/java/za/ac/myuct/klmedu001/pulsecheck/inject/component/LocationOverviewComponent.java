package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.LocationOverviewModule;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;

/**
 * dagger component for {@link za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview}
 * Created by eduardokolomajr on 2015/05/03.
 */
@Component(modules = LocationOverviewModule.class)
public interface LocationOverviewComponent {
    LocationOverview pulse();
}
