package za.ac.myuct.klmedu001.pulsecheck.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Overview of a location for displaying in the home feeds
 * Created by eduardokolomajr on 2015/04/25.
 */
public class LocationOverview implements Parcelable {
    public final String title;
    public final String link;
    public final String imageLink;

    public LocationOverview(String title, String link, String imageLink) {
        this.title = title;
        this.link = link;
        this.imageLink = imageLink;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.link);
        dest.writeString(this.imageLink);
    }

    private LocationOverview(Parcel in) {
        this.title = in.readString();
        this.link = in.readString();
        this.imageLink = in.readString();
    }

    public static final Parcelable.Creator<LocationOverview> CREATOR = new Parcelable.Creator<LocationOverview>() {
        public LocationOverview createFromParcel(Parcel source) {return new LocationOverview(source);}

        public LocationOverview[] newArray(int size) {return new LocationOverview[size];}
    };
}
