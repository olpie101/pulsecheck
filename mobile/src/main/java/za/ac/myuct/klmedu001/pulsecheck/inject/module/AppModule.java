package za.ac.myuct.klmedu001.pulsecheck.inject.module;

import android.content.Context;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import za.ac.myuct.klmedu001.pulsecheck.constant.App;

/**
 * Created by eduardokolomajr on 2015/04/13.
 * App level Module
 */
@Module
public class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @Singleton public Context provideApplicationContext(){
        return app;
    }

    @Provides @Singleton public Bus provideEventBus() { return new Bus(ThreadEnforcer.ANY);}
}
