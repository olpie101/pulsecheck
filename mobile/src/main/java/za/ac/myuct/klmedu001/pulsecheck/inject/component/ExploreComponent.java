package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import android.support.v7.widget.RecyclerView;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.adapter.PlacesListAdapter;
import za.ac.myuct.klmedu001.pulsecheck.fragment.ExploreFragment;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.ExploreFragmentModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerFragment;
import za.ac.myuct.klmedu001.pulsecheck.web.ExplorePulsesLoader;

/**
 * {@link ExploreFragment} Fragment DI {@link Component}
 * Created by eduardokolomajr on 2015/04/17.
 */
@PerFragment
@Component(dependencies = MainActivityComponent.class, modules = {ExploreFragmentModule.class})
public interface ExploreComponent {
    void inject(ExploreFragment exploreFragment);


    PlacesListAdapter placesListAdapter();

    RecyclerView.LayoutManager layoutManager();

    ExplorePulsesLoader explorePulsesLoader();
}
