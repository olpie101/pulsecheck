package za.ac.myuct.klmedu001.pulsecheck.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

import javax.inject.Inject;

import za.ac.myuct.klmedu001.pulsecheck.fragment.ExploreFragment;
import za.ac.myuct.klmedu001.pulsecheck.util.EntryPair;

/**
 * {@link SectionsPagerAdapter} for {@link android.support.v4.app.FragmentPagerAdapter}
 * in {@link za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity}
 * Created by eduardokolomajr on 2015/04/16.
 */
public class MainActivitySectionsAdapter extends SectionsPagerAdapter {
    @Inject
        public MainActivitySectionsAdapter(FragmentManager fm, List<EntryPair<String, Class>> fragments) {
        super(fm, fragments);
    }


        @Override
    public Fragment getItem(int position) {
        String className = fragments.get(position).second.getName();
        if(className.equals(ExploreFragment.class.getName())){
            return new ExploreFragment();
        }
        return null;
    }
}
