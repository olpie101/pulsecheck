package za.ac.myuct.klmedu001.pulsecheck.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.constant.App;
import za.ac.myuct.klmedu001.pulsecheck.fragment.PulseDetailFragment;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.AppComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.DaggerPulseDetailActivityComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.PulseDetailActivityComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.PulseActivityModule;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;

public class PulseDetailActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse_detail);
        if(getIntent() != null){
            LocationOverview location = getIntent().getParcelableExtra("pulse");

            getActivityComponent().fragmentManager().beginTransaction()
                    .add(R.id.pulse_container, PulseDetailFragment.newInstance(location))
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pulse_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private AppComponent getAppComponent(){
        return ((App)getApplication()).getAppComponent();
    }

    private Context getAppContext(){
        return getAppComponent().context();
    }

    private PulseDetailActivityComponent getActivityComponent(){
        return DaggerPulseDetailActivityComponent.builder().appComponent(getAppComponent())
                .pulseActivityModule(new PulseActivityModule(this))
                .build();
    }
}
