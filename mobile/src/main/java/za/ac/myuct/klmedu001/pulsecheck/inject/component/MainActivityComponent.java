package za.ac.myuct.klmedu001.pulsecheck.inject.component;

import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;

import dagger.Component;
import za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity;
import za.ac.myuct.klmedu001.pulsecheck.adapter.MainActivitySectionsAdapter;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.MainActivityModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.scope.PerActivity;

/**
 * Links {@link MainActivity} and {@link MainActivityModule} for Dagger dependency injection.<br/>
 * Created by eduardokolomajr on 2015/04/14.
 */
@PerActivity
@Component(dependencies = AppComponent.class, modules = MainActivityModule.class)
public interface MainActivityComponent {
    void inject(MainActivity mainActivity);
    //Exposed to sub-graphs.
    Activity activity();

    ActionBar actionBar();

    FragmentManager fragmentManager();

//    SectionsPagerAdapter sectionPagesAdapter();

    MainActivitySectionsAdapter mainActivitySectionsAdapter();
}
