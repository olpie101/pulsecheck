package za.ac.myuct.klmedu001.pulsecheck.fragment;

import android.animation.ValueAnimator;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import rx.Observable;
import rx.android.app.AppObservable;
import rx.android.app.support.RxFragment;
import rx.android.lifecycle.LifecycleEvent;
import rx.android.lifecycle.LifecycleObservable;
import rx.functions.Func1;
import za.ac.myuct.klmedu001.pulsecheck.R;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.DaggerPulseDetailFragmentComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.component.PulseDetailFragmentComponent;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.LocationOverviewModule;
import za.ac.myuct.klmedu001.pulsecheck.inject.module.PulseDetailFragmentModule;
import za.ac.myuct.klmedu001.pulsecheck.model.LocationOverview;
import za.ac.myuct.klmedu001.pulsecheck.web.PulseDetailsLoader;

/**
 * A placeholder fragment containing a simple view.
 */
public class PulseDetailFragment extends RxFragment /*implements OnMapReadyCallback*/ {
    public static final String PULSE_ID = "pulse";
    private final String TAG = getClass().getSimpleName();
    private LocationOverview pulse;
    @InjectView(R.id.iv_pulse_detail_img)
    ImageView image;
    @InjectView(R.id.tv_pulse_detail_title)
    TextView tvTitle;
    @InjectView(R.id.tv_pulse_detail_opening)
    TextView tvOpening;
    @InjectView(R.id.tv_pulse_detail_description)
    TextView tvDescription;
    @InjectView(R.id.tv_pulse_detail_followers)
    TextView tvFollowers;
//    @InjectView(R.id.ib_pulse_detail_map_toggle)
//    ImageButton ibMapToggle;
    @InjectView(R.id.mv_pulse_map)
    MapView pulseMapView;

    GoogleMap pulseMap;
    private Observable<PulseDetailsLoader> pulseDetailsLoader;

    public static PulseDetailFragment newInstance(LocationOverview pulse){
        PulseDetailFragment fragment = new PulseDetailFragment();
        Bundle b= new Bundle();
        b.putParcelable(PULSE_ID, pulse);
        fragment.setArguments(b);
        return fragment;
    }

    public PulseDetailFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
            pulse = getArguments().getParcelable(PULSE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_pulse_detail, container, false);
        ButterKnife.inject(this, v);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Picasso.with(getActivity()).load(pulse.imageLink).resize(width, (int)(width*0.66)).centerCrop().into(image);
        tvTitle.setText(pulse.title);
        pulseMapView.onCreate(savedInstanceState);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d(TAG, "Initializing maps");
        pulseMap = pulseMapView.getMap();
        pulseMap.getUiSettings().setAllGesturesEnabled(false);
        pulseMap.getUiSettings().setMyLocationButtonEnabled(false);
        MapsInitializer.initialize(this.getActivity());

        pulseDetailsLoader =
                bindLifecycle(Observable.just(getFragmentComponent().pulseDetailsLoader()),
                        LifecycleEvent.DESTROY_VIEW);

        pulseDetailsLoader
                .flatMap(PulseDetailsLoader::getDescription)
                .subscribe(tvDescription::setText);

        pulseDetailsLoader
                .flatMap(PulseDetailsLoader::getFollowers)
                .subscribe(numFollowers -> tvFollowers.setText(numFollowers + " followers"));

        pulseDetailsLoader
                .flatMap(PulseDetailsLoader::getMapCoords)
                .map(latLng -> new MarkerOptions().position(latLng).title(pulse.title))
                .map(addMarkerToMap())
                .subscribe(latLng ->
                        pulseMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.5f)));
    }

    @Override
    public void onResume() {
        if(pulseMapView != null)
            pulseMapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(pulseMapView != null)
            pulseMapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(pulseMapView != null)
            pulseMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(pulseMapView != null)
            pulseMapView.onDestroy();
    }

    private Func1<MarkerOptions, LatLng> addMarkerToMap() {
        return markerOptions -> {

            if(pulseMap != null) {
                pulseMap.addMarker(markerOptions);
            }else
                Log.d(TAG, "Pulse map is null");

            return markerOptions.getPosition();
        };
    }

    @OnClick(R.id.ib_pulse_detail_map_toggle)
    void onMapToggleClick(){
        Log.d(TAG, "Map toggle clicked");
        int startHeight = pulseMapView.getMeasuredHeight();
        int endHeight = 0;
        if(startHeight == 0)
            endHeight = getResources().getDimensionPixelSize(R.dimen.mapview_pulse_details_max_height);

        //create value animator and add listener to animate height of map
        ValueAnimator heightAnimator = ValueAnimator.ofInt(startHeight, endHeight);
        heightAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        heightAnimator.setDuration(300);
        heightAnimator.addUpdateListener(animation -> {
                ViewGroup.LayoutParams layoutParams = pulseMapView.getLayoutParams();
                layoutParams.height = (int) animation.getAnimatedValue();
                pulseMapView.setLayoutParams(layoutParams);}
        );
        heightAnimator.start();
    }

    public PulseDetailFragmentComponent getFragmentComponent(){
        return DaggerPulseDetailFragmentComponent.builder()
                .locationOverviewModule(new LocationOverviewModule(pulse))
                .pulseDetailFragmentModule(new PulseDetailFragmentModule(this)).build();
    }

    /**
     * Bind observable to fragment lifecycle. Ensures that observable only emits as long a fragment
     * is in a valid state
     * @param observable Observable to bind
     * @param lifecycleEvent bind until given lifecycle event
     * @param <T> Type provided observable shall emit
     * @return Observable based on given observable bound until given lifecycle
     */
    private <T> Observable<T> bindLifecycle(Observable<T> observable, LifecycleEvent lifecycleEvent){
        Observable<T> obs = AppObservable.bindFragment(this, observable);
        return LifecycleObservable.bindUntilLifecycleEvent(lifecycle(), obs, lifecycleEvent);
    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        Log.d(TAG, "map received");
//        pulseMap = googleMap;
//        Log.d(TAG, "map set");
//        pulseDetailsLoader
//                .flatMap(PulseDetailsLoader::getMapCoords)
//                .map(latLng -> new MarkerOptions().position(latLng).title(pulse.title))
//                .map(addMarkerToMap())
//                .subscribe(latLng ->
//                        pulseMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10.0f)));
//    }
}
