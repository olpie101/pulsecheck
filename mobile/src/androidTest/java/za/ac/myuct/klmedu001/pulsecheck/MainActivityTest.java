package za.ac.myuct.klmedu001.pulsecheck;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.LargeTest;

import za.ac.myuct.klmedu001.pulsecheck.activity.MainActivity;

/**
 * Test for {@link MainActivity}
 * Created by eduardokolomajr on 2015/04/15.
 */
@LargeTest
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
    }


}
